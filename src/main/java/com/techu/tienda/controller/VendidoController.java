package com.techu.tienda.controller;

import com.techu.tienda.model.ProductoModel;
import com.techu.tienda.model.VendidoModel;
import com.techu.tienda.service.ProductoService;
import com.techu.tienda.service.VendidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Calendar;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
@RequestMapping ("${url.base}")
public class VendidoController {

    @Autowired
    VendidoService vendidoService;
    ProductoService productoService;

    // METODO EXRA
    // Este método realiza una venta de un vendido, lo elimina de la base de datos de vendidos y
    // lo inserta en la BBDD de vendidos.
    @PostMapping  ("/venta/{id}")
    public ResponseEntity <Boolean> vender (@PathVariable String id)
    {
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> pm = productoService.findById(id);
            VendidoModel vm = new VendidoModel();

            vm.setId(pm.get().getId());
            vm.setDescripcion(pm.get().getDescripcion());
            vm.setPrecio(pm.get().getPrecio());
            Calendar cal = Calendar.getInstance();
            vm.setFechaVenta(cal.getTime());

            return new ResponseEntity(vendidoService.deleteById(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping ("/vendidos")
    public ResponseEntity<VendidoModel> getVendidos() {
        Long numRegistros  = vendidoService.count();
        if(numRegistros >0) {
            return new ResponseEntity(vendidoService.findAll(), HttpStatus.OK );
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
    }


    @GetMapping("/vendidos/{id}" )
    public ResponseEntity <VendidoModel> getProductoId(@PathVariable String id) {
        Optional<VendidoModel> pr = vendidoService.findById(id);
        if(pr.isPresent()) {
            return new ResponseEntity(vendidoService.findById(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/vendidos")
    public ResponseEntity <VendidoModel> postVendidos(@RequestBody VendidoModel newProducto) {
        if (vendidoService.findById(newProducto.getId()).isPresent())
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity(vendidoService.save(newProducto), HttpStatus.CREATED);
        }
    }

    // METODO EXTRA
    //En este método no le pasamos el id en el cuerpo, sino que lo genera automaticamente y así evitamos problemas en la BBDD
    @PostMapping("/vendidosAutoId")
    public ResponseEntity <VendidoModel> postVendidosAutoId(@RequestBody VendidoModel newProducto) {
        return new ResponseEntity(vendidoService.saveAutoId(newProducto), HttpStatus.CREATED);
    }


    @PutMapping ("/vendidos")
    public ResponseEntity <VendidoModel>  putVendidos (@RequestBody VendidoModel vendidoToUpdate){
        if (vendidoService.findById(vendidoToUpdate.getId()).isPresent()) {
            return new ResponseEntity(vendidoService.save(vendidoToUpdate), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping ("/vendidos/{id}")
    public ResponseEntity <VendidoModel>  putVendidosById (@RequestBody VendidoModel vendidoToUpdate,@PathVariable String id ){
        if (vendidoService.findById(vendidoToUpdate.getId()).isPresent()) {
            if (vendidoToUpdate.getId().equals(id)) {      //NO PERMITO ACTUALIZAR EL ID
                return new ResponseEntity(vendidoService.save(vendidoToUpdate), HttpStatus.OK);
            }
            else {
                return new ResponseEntity(null, HttpStatus.NOT_FOUND);
            }
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }


    // METODO EXTRA
    //En este método no le pasamos el id en el cuerpo, sino que lo toma de la petición y así evitamos que nos modifiquen el ID
    @PutMapping ("/vendidosAutoId/{id}")
    public ResponseEntity <VendidoModel>  putVendidosAutoIdById (@RequestBody VendidoModel vendidoToUpdate,@PathVariable String id ){
        vendidoToUpdate.setId(id);
        if (vendidoService.existsVendido(vendidoToUpdate)) {
            return new ResponseEntity(vendidoService.save(vendidoToUpdate), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    // METODO EXTRA
    // Este método lo utilizamos para generar automaticamente campos de prueba para la BBDD
    @PostMapping ("/vendidosRestartBBDD/{numVendidos}")
    public ResponseEntity <Boolean> vendidosRestartBBDD (@PathVariable int numVendidos) {
        vendidoService.deleteAll();
        VendidoModel vendido = new VendidoModel ();
        for (int i = 1; i < numVendidos+1; i++)
        {
            System.out.println("Creando vendido " + i);
            vendido.setId (Integer.toString(i) );
            vendido.setDescripcion("Este es el vendido " + Integer.toString(i));
            Double d=Double.valueOf(i);
            vendido.setPrecio(d);
            Calendar cal = Calendar.getInstance();
            vendido.setFechaVenta(cal.getTime());
            vendidoService.save (vendido);
        }
        return new ResponseEntity(true, HttpStatus.CREATED);
    }


    @DeleteMapping ("/vendidos")
    public ResponseEntity <Boolean> deleteVendidos (@RequestBody VendidoModel vendidoToDelete) {
        if (vendidoService.existsVendido (vendidoToDelete)) {
            return new ResponseEntity(vendidoService.delete(vendidoToDelete), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping ("/vendidos/{id}")
    public ResponseEntity <Boolean> deleteVendidosById (@PathVariable String id) {
        if (vendidoService.findById(id).isPresent()) {
            return new ResponseEntity(vendidoService.deleteById(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
    }

    // METODO EXTRA
    // Este método lo utilizamos para borrar completamente la BBDD
    @DeleteMapping ("/vendidosAll/")
    public ResponseEntity <Boolean>  deleteAll () {
        return new ResponseEntity(vendidoService.deleteAll(), HttpStatus.OK);
    }


    @PatchMapping ("/vendidos/{id}")
    public ResponseEntity<VendidoModel> patchVendidos (@RequestBody VendidoModel vendidoToUpdate,@PathVariable String id ){
        if (vendidoService.findById(id).isPresent()) {
            Optional <VendidoModel> pm = vendidoService.findById(id);

            if (vendidoToUpdate.getPrecio() != null) {
                pm.get().setPrecio (vendidoToUpdate.getPrecio());
            }

            if (vendidoToUpdate.getDescripcion() != null) {
                pm.get().setDescripcion (vendidoToUpdate.getDescripcion());
            }
            return new ResponseEntity (vendidoService.save(pm.get()), HttpStatus.OK);
        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }


    @PatchMapping ("/vendidosPrecio/{id}")
    public ResponseEntity<VendidoModel> patchVendidosPrecio (@RequestBody VendidoModel vendidoToUpdate,@PathVariable String id ){
        if (vendidoService.findById(id).isPresent()) {
            Optional <VendidoModel> pm = vendidoService.findById(id);
            pm.get().setPrecio(vendidoToUpdate.getPrecio());
            return new ResponseEntity (vendidoService.save(pm.get()), HttpStatus.OK);

        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping ("/vendidosDescripcion/{id}")
    public ResponseEntity<VendidoModel> patchVendidosDescripcion (@RequestBody VendidoModel vendidoToUpdate,@PathVariable String id ){
        if (vendidoService.findById(id).isPresent()) {
            Optional <VendidoModel> pm = vendidoService.findById(id);
            pm.get().setDescripcion(vendidoToUpdate.getDescripcion());
            return new ResponseEntity (vendidoService.save(pm.get()), HttpStatus.OK);
        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }
}
