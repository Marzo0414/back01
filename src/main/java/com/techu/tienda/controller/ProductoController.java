package com.techu.tienda.controller;

import com.techu.tienda.model.ProductoModel;
import com.techu.tienda.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.PATCH})
@RequestMapping ("${url.base}")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping ("/productos")
    public ResponseEntity<ProductoModel> getProductos() {
        Long numRegistros  = productoService.count();
        if(numRegistros >0) {
            return new ResponseEntity(productoService.findAll(), HttpStatus.OK );
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
    }


    @GetMapping("/productos/{id}" )
    public ResponseEntity <ProductoModel> getProductoId(@PathVariable String id) {
        Optional<ProductoModel> pr = productoService.findById(id);
        if(pr.isPresent()) {
            return new ResponseEntity(productoService.findById(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(null, HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/productos")
    public ResponseEntity <ProductoModel> postProductos(@RequestBody ProductoModel newProducto) {
        if (productoService.findById(newProducto.getId()).isPresent())
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity(productoService.save(newProducto), HttpStatus.CREATED);
        }
    }

    // METODO EXTRA
    //En este método no le pasamos el id en el cuerpo, sino que lo genera automaticamente y así evitamos problemas en la BBDD
    @PostMapping("/productosAutoId")
    public ResponseEntity <ProductoModel> postProductosAutoId(@RequestBody ProductoModel newProducto) {
        return new ResponseEntity(productoService.saveAutoId(newProducto), HttpStatus.CREATED);
    }


    @PutMapping ("/productos")
    public ResponseEntity <ProductoModel>  putProductos (@RequestBody ProductoModel productoToUpdate){
        if (productoService.findById(productoToUpdate.getId()).isPresent()) {
            return new ResponseEntity(productoService.save(productoToUpdate), HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping ("/productos/{id}")
    public ResponseEntity <ProductoModel>  putProductosById (@RequestBody ProductoModel productoToUpdate,@PathVariable String id ){
        if (productoService.findById(productoToUpdate.getId()).isPresent()) {
            if (productoToUpdate.getId().equals(id)) {      //NO PERMITO ACTUALIZAR EL ID
                return new ResponseEntity(productoService.save(productoToUpdate), HttpStatus.OK);
            }
            else {
                return new ResponseEntity(null, HttpStatus.NOT_FOUND);
            }
        }
        else
        {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }


    // METODO EXTRA
    //En este método no le pasamos el id en el cuerpo, sino que lo toma de la petición y así evitamos que nos modifiquen el ID
    @PutMapping ("/productosAutoId/{id}")
    public ResponseEntity <ProductoModel>  putProductosAutoIdById (@RequestBody ProductoModel productoToUpdate,@PathVariable String id ){
        productoToUpdate.setId(id);
        if (productoService.existsProducto(productoToUpdate)) {
            return new ResponseEntity(productoService.save(productoToUpdate), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(null, HttpStatus.NOT_FOUND);
        }
    }

    // METODO EXTRA
    // Este método lo utilizamos para generar automaticamente campos de prueba para la BBDD
    @PostMapping ("/productosRestartBBDD/{numProductos}")
    public ResponseEntity <Boolean> productosRestartBBDD (@PathVariable int numProductos) {
        productoService.deleteAll();
        ProductoModel producto = new ProductoModel ();
        for (int i = 1; i < numProductos+1; i++)
        {
            System.out.println("Creando producto " + i);
            producto.setId (Integer.toString(i) );
            producto.setDescripcion("Este es el producto " + Integer.toString(i));
            Double d=Double.valueOf(i);
            producto.setPrecio(d);
            productoService.save (producto);
        }
        return new ResponseEntity(true, HttpStatus.CREATED);
    }


    @DeleteMapping ("/productos")
    public ResponseEntity <Boolean> deleteProductos (@RequestBody ProductoModel productoToDelete) {
        if (productoService.existsProducto (productoToDelete)) {
            return new ResponseEntity(productoService.delete(productoToDelete), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping ("/productos/{id}")
    public ResponseEntity <Boolean> deleteProductosById (@PathVariable String id) {
        if (productoService.findById(id).isPresent()) {
            return new ResponseEntity(productoService.deleteById(id), HttpStatus.OK);
        }
        else {
            return new ResponseEntity(false, HttpStatus.NOT_FOUND);
        }
    }

    // METODO EXTRA
    // Este método lo utilizamos para borrar completamente la BBDD
    @DeleteMapping ("/productosAll/")
    public ResponseEntity <Boolean>  deleteAll () {
        return new ResponseEntity(productoService.deleteAll(), HttpStatus.OK);
    }


    @PatchMapping ("/productos/{id}")
    public ResponseEntity<ProductoModel> patchProductos (@RequestBody ProductoModel productoToUpdate,@PathVariable String id ){
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> pm = productoService.findById(id);

            if (productoToUpdate.getPrecio() != null) {
                pm.get().setPrecio (productoToUpdate.getPrecio());
            }

            if (productoToUpdate.getDescripcion() != null) {
                pm.get().setDescripcion (productoToUpdate.getDescripcion());
            }
            return new ResponseEntity (productoService.save(pm.get()), HttpStatus.OK);
        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }


    @PatchMapping ("/productosPrecio/{id}")
    public ResponseEntity<ProductoModel> patchProductosPrecio (@RequestBody ProductoModel productoToUpdate,@PathVariable String id ){
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> pm = productoService.findById(id);
            pm.get().setPrecio(productoToUpdate.getPrecio());
            return new ResponseEntity (productoService.save(pm.get()), HttpStatus.OK);

        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }

    @PatchMapping ("/productosDescripcion/{id}")
    public ResponseEntity<ProductoModel> patchProductosDescripcion (@RequestBody ProductoModel productoToUpdate,@PathVariable String id ){
        if (productoService.findById(id).isPresent()) {
            Optional <ProductoModel> pm = productoService.findById(id);
            pm.get().setDescripcion(productoToUpdate.getDescripcion());
            return new ResponseEntity (productoService.save(pm.get()), HttpStatus.OK);
        }
        else {
            return new ResponseEntity (null, HttpStatus.NOT_FOUND);
        }
    }
}
