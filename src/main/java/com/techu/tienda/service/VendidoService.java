package com.techu.tienda.service;


import com.techu.tienda.model.VendidoModel;
import com.techu.tienda.repository.VendidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class VendidoService {

    @Autowired
    VendidoRepository vendidoRepository;

    // Read
    public List<VendidoModel> findAll () {
        return vendidoRepository.findAll();
    }


    // Read by Id
    public Optional<VendidoModel> findById (String id) {
        return vendidoRepository.findById(id);
    }


    // Create
    public VendidoModel save (VendidoModel vendido) {
        return vendidoRepository.save (vendido);
    }

    // Create Id autogenerado
    public VendidoModel saveAutoId (VendidoModel vendido) {
        vendidoRepository.count ();
        String newId = Long.toString(vendidoRepository.count () + 1);
        vendido.setId(newId);
        return vendidoRepository.save (vendido);
    }


    // Count
    public Long count (){
        return vendidoRepository.count();
    }

    // Exists Vendido
    public boolean existsVendido (VendidoModel vendido) {
        if (vendidoRepository.existsById(vendido.getId()))
            return true;
        else
            return false;
    }


    // Delete
    public boolean delete (VendidoModel vendido) {
        try {
            vendidoRepository.delete(vendido);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    // Delete By Id
     public boolean deleteById (String id) {
         try {
             vendidoRepository.deleteById(id);
             return true;
         } catch (Exception e) {
             return false;
         }
     }


     // Delete All
    public boolean deleteAll () {
        try {
            vendidoRepository.deleteAll();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
