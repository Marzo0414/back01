package com.techu.tienda.service;

import com.techu.tienda.model.ProductoModel;
import com.techu.tienda.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;


@Service
public class ProductoService {

    @Autowired
    ProductoRepository productoRepository;

    // Read
    public List<ProductoModel> findAll () {
        return productoRepository.findAll();
    }


    // Read by Id
    public Optional<ProductoModel> findById (String id) {
        return productoRepository.findById(id);
    }

    // Count
    public Long count (){
        return productoRepository.count();
    }


    // Create
    public ProductoModel save (ProductoModel producto) {
        return productoRepository.save (producto);
    }

    // Create Id autogenerado
    public ProductoModel saveAutoId (ProductoModel producto) {
        productoRepository.count ();
        String newId = Long.toString(productoRepository.count () + 1);
        producto.setId(newId);
        return productoRepository.save (producto);
    }

    // Exists Producto
    public boolean existsProducto (ProductoModel producto) {
        if (productoRepository.existsById(producto.getId()))
            return true;
        else
            return false;
    }


    // Delete
    public boolean delete (ProductoModel producto) {
        try {
            productoRepository.delete(producto);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    // Delete By Id
     public boolean deleteById (String id) {
         try {
             productoRepository.deleteById(id);
             return true;
         } catch (Exception e) {
             return false;
         }
     }


     // Delete All
    public boolean deleteAll () {
        try {
            productoRepository.deleteAll();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
