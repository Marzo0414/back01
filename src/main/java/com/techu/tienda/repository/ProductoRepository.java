package com.techu.tienda.repository;

import com.techu.tienda.model.ProductoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {
}
