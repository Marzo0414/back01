package com.techu.tienda.repository;

import com.techu.tienda.model.VendidoModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendidoRepository extends MongoRepository<VendidoModel, String> {
}
